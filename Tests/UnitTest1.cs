﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using EntitiesLayer;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        DalManager dal = DalManager.Instance;

        [TestMethod]
        public void TestCharacter()
        {
            Character character = new Character("", "", 100, CharacterType.LEADER, 100, 100, new House(1, "", 200));
            int id = dal.addCharacter(character);
            Assert.AreNotEqual(id, -1);
            dal.deleteCharacter(id);
            Assert.IsNull(dal.getCharacterById(id));
        }

        [TestMethod]
        public void TestHouse()
        {
            House house = new House("", 250);
            int id = dal.addHouse(house);
            Assert.AreNotEqual(id, -1);
            dal.deleteHouse(id);
            Assert.IsNull(dal.getHouseById(id));
        }

        [TestMethod]
        public void TestTerritory()
        {
            Territory territory = new Territory(TerritoryType.LAND, new House(1, "", 200));
            int id = dal.addTerritory(territory);
            Assert.AreNotEqual(id, -1);
            dal.deleteTerritory(id);
            Assert.IsNull(dal.getTerritoryById(id));
        }

        [TestMethod]
        public void TestFight()
        {
            House house = new House("", 250);
            House house2 = new House("", 100);
            War war = new War("");
            Fight fight = new Fight("", house, house2, house, war);
            int id = dal.addFight(fight);
            Assert.AreNotEqual(id, -1);
            dal.deleteFight(id);
            Assert.IsNull(dal.getFightById(id));
        }

        [TestMethod]
        public void TestWar()
        {
            War war = new War("");
            int id = dal.addWar(war);
            Assert.AreNotEqual(id, -1);
            dal.deleteWar(id);
            Assert.IsNull(dal.getWarById(id));
        }
    }
}
