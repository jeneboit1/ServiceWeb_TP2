﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public enum CharacterType
    {
        WARRIOR,
        WITCH,
        TACTITIAN,
        LEADER,
        LOSER
    }

    public class Characteristics
    {
        public uint PV { get; set; }
        public CharacterType Type { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public Characteristics(uint pv, CharacterType type, int bravoury, int crazyness)
        {
            PV = pv;
            Type = type;
            Bravoury = bravoury;
            Crazyness = crazyness;
        }
    }

    public class Character : EntityObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Characteristics Characteristics { get; set; }
        public House House { get; set; }
        public List<RelationShip> RelationShips { get; set; }

        public Character(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            Characteristics = new Characteristics(100, 0, 100, 50);
            House = new House("Faceless", 0);
            RelationShips = new List<RelationShip>();
        }

        public Character(string firstName, string lastName, uint pv, CharacterType type, int bravoury, int crazyness, House house)
        {
            FirstName = firstName;
            LastName = lastName;
            Characteristics = new Characteristics(pv, type, bravoury, crazyness);
            House = house;
            RelationShips = new List<RelationShip>();
        }

        public Character(int id, string firstName, string lastName, uint pv, CharacterType type, int bravoury, int crazyness, House house)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Characteristics = new Characteristics(pv, type, bravoury, crazyness);
            House = house;
            RelationShips = new List<RelationShip>();
        }

        public Character(int id,  string firstName, string lastName, uint pv, CharacterType type, int bravoury, int crazyness, House house, List<RelationShip> relationShips) : this(id, firstName, lastName, pv, type, bravoury, crazyness, house)
        {
            RelationShips = relationShips;
        }
        

        public void AddRelatives(Character c, RelationshipType rel)
        {
            RelationShips.Add(new RelationShip(c, rel));
        }

        public override string ToString()
        {
            return "I am " + FirstName + " " + LastName + " and  I have " + Characteristics.PV + " pv.";
        }
    }
}
