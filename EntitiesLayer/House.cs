﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class House : EntityObject
    {
        public string Name { get; set; }
        public int NbUnits { get; set; }

        public House(string name, int nbUnits)
        {
            ID = 0;
            Name = name;
            NbUnits = nbUnits;
        }

        public House(int id, string name, int nbUnits)
        {
            ID = id;
            Name = name;
            NbUnits = nbUnits;
        }
    }
}
