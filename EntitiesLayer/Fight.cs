﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Fight : EntityObject
    {
        public String Name { get; set; }
        public House Challenger1 { get; set; }
        public House Challenger2 { get; set; }
        public House Winner { get; set; }
        public War War { get; set; }
        public Territory Territory { get; set; }

        public Fight(String name, House challenger1, House challenger2, War war, Territory territory)
        {
            ID = 0;
            Name = name;
            Challenger1 = challenger1;
            Challenger2 = challenger2;
            double prob;
            Random rnd = new Random();
            if (challenger1.NbUnits > challenger2.NbUnits)
            {
                prob = (double)challenger2.NbUnits / (double)challenger1.NbUnits;
                if (rnd.NextDouble() <= prob)
                {
                    Winner = challenger2;
                }
                else
                {
                    Winner = challenger1;
                }
            }
            else
            {
                prob = challenger1.NbUnits / challenger2.NbUnits;
                if (rnd.NextDouble() <= prob)
                {
                    Winner = challenger1;
                }
                else
                {
                    Winner = challenger2;
                }
            }
            War = war;
            Territory = territory;
        }

        public Fight(String name, House challenger1, House challenger2, House winner, War war, Territory territory)
        {
            Name = name;
            Challenger1 = challenger1;
            Challenger2 = challenger2;
            Winner = winner;
            War = war;
            Territory = territory;
        }

        public Fight(int id, String name, House challenger1, House challenger2, House winner,  War war, Territory territory)
        {
            ID = id;
            Name = name;
            Challenger1 = challenger1;
            Challenger2 = challenger2;
            Winner = winner;
            War = war;
            Territory = territory;
        }
    }
}
