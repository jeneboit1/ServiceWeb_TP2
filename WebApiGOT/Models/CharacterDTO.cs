﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class CharacteristicsDTO
    {
        public uint PV { get; set; }
        public CharacterType Type { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public CharacteristicsDTO()
        {
            PV = 0;
            Type = CharacterType.LEADER;
            Bravoury = 0;
            Crazyness = 0;
        }

        public CharacteristicsDTO(Characteristics c)
        {
            PV = c.PV;
            Type = c.Type;
            Bravoury = c.Bravoury;
            Crazyness = c.Crazyness;
        }
    }

    public class CharacterDTO
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public HouseDTO House { get; set; }
        public CharacteristicsDTO Characteristics { get; set; }
        public List<RelationShipDTO> RelationShips { get; set; }

        public CharacterDTO()
        {
            ID = 0;
            FirstName = "";
            LastName = "";
            House = new HouseDTO();
            Characteristics = new CharacteristicsDTO();
            RelationShips = new List<RelationShipDTO>();
        }

        public CharacterDTO(Character c)
        {
            ID = c.ID;
            FirstName = c.FirstName;
            LastName = c.LastName;
            House = new HouseDTO(c.House);
            Characteristics = new CharacteristicsDTO(c.Characteristics);
            RelationShips = new List<RelationShipDTO>();
            foreach(var r in c.RelationShips)
            {
                RelationShips.Add(new RelationShipDTO(new CharacterDTO(r.Character), r.Type));
            }
        }
    }
 }