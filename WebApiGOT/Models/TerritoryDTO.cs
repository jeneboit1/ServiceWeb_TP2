﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class TerritoryDTO
    {
        public int ID { get; set; }
        public TerritoryType Type { get; set; }
        public HouseDTO Owner { get; set; }
        public string Name { get; set; }

        public TerritoryDTO(Territory t)
        {
            ID = t.ID;
            Type = t.Type;
            Owner = new HouseDTO(t.Owner);
            Name = t.Name;
        }
        public TerritoryDTO()
        {
            ID = 0;
            Type = 0;
            Owner = null;
            Name = "";
        }
    }
}