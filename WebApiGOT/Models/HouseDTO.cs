﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiGOT.Models
{
    public class HouseDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int NbUnits { get; set; }
        public HouseDTO()
        {
            ID = 0;
            Name = "";
            NbUnits = 0;
        }
        public HouseDTO(House h)
        {
            ID = h.ID;
            Name = h.Name;
            NbUnits = h.NbUnits;
        }
    }
}