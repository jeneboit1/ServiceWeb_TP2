﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiGOT.Models;
using EntitiesLayer;

namespace WebApiGOT.Controllers
{
    public class CharacterController : ApiController
    {
        public List<CharacterDTO> GetAllCharacters()
        {
            List<CharacterDTO> list = new List<CharacterDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            foreach (Character charac in busi.CharactersList())
            {
                list.Add(new CharacterDTO(charac));
            }

            return list;
        }

        public CharacterDTO GetCharacter(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            CharacterDTO carac = new CharacterDTO(busi.getCharacterById(id));
            return carac;
        }

        public void PostAddCharacter(CharacterDTO c)
        {
            Character cha = new Character(c.FirstName, c.LastName, c.Characteristics.PV, c.Characteristics.Type, c.Characteristics.Bravoury, c.Characteristics.Crazyness, new House(c.House.ID, c.House.Name, c.House.NbUnits));
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.addCharacter(cha);
        }

        public void PutEditCharacter(CharacterDTO c)
        {
            Character cha = new Character(c.ID, c.FirstName, c.LastName, c.Characteristics.PV, c.Characteristics.Type, c.Characteristics.Bravoury, c.Characteristics.Crazyness, new House(c.House.ID, c.House.Name, c.House.NbUnits));
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.updateCharacter(cha);
        }
        
        public void DeleteCharacter(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.deleteCharacater(id);
        }
    }
}
