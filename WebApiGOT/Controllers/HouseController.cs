﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiGOT.Models;
using BusinessLayer;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntitiesLayer;

namespace WebApiGOT.Controllers
{
    public class HouseController : ApiController
    {
        // GET: api/House
        public List<HouseDTO> GetAllHouse()
        {
            List<HouseDTO> list = new List<HouseDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            foreach (EntitiesLayer.House charac in busi.HousesList())
            {
                list.Add(new HouseDTO(charac));
            }
            return list;
        }

        // GET: api/House/5
        public HouseDTO GetHouse(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            HouseDTO house = new HouseDTO(busi.getHouseById(id));
            return house;
        }

        // POST: api/House
        public void PostAddHouse(HouseDTO house)
        {
            House ho = new House(house.Name, house.NbUnits);
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.addHouse(ho);
        }

        // PUT: api/House/5
        public void PutEditHouse(HouseDTO house)
        {
            House cha = new House(house.ID,house.Name, house.NbUnits);
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.updateHouse(cha);
        }

        // DELETE: api/House/5
        public void DeleteHouse(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.deleteHouse(id);
        }
    }
}