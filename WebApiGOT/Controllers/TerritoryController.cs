﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiGOT.Models;
using BusinessLayer;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntitiesLayer;

namespace WebApiGOT.Controllers
{
    public class TerritoryController : ApiController
    {
        // GET: api/Territory
        public List<TerritoryDTO> GetAllTerritory()
        {
            List<TerritoryDTO> list = new List<TerritoryDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            foreach (EntitiesLayer.Territory territory in busi.TerritoryList())
            {
                list.Add(new TerritoryDTO(territory));
            }
            return list;
        }

        // GET: api/Territory/5
        public TerritoryDTO Get(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            return new TerritoryDTO(busi.getTerritoryById(id));
        }

        // POST: api/Territory
        public void Post(TerritoryDTO terr)
        {
            House h = new House(terr.Owner.ID,terr.Owner.Name,terr.Owner.NbUnits);
            Territory Terry = new Territory(terr.Type,h,terr.Name);
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.addTerritory(Terry);
        }

        // PUT: api/Territory/5
        public void Put(TerritoryDTO terr)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            House h = new House(terr.Owner.ID, terr.Owner.Name, terr.Owner.NbUnits);
            Territory Terry = new Territory(terr.ID, terr.Type, h, terr.Name);
            busi.updateTerritory(Terry);
        }

        // DELETE: api/Territory/5
        public void Delete(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.deleteTerritory(id);

        }
    }
}
