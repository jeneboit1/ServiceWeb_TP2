﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiGOT.Models;
using BusinessLayer;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntitiesLayer;


namespace WebApiGOT.Controllers
{
    public class WarController : ApiController
    {
        // GET: api/War
        public List<WarDTO> GetAllWar()
        {
            List<WarDTO> list = new List<WarDTO>();
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            foreach (EntitiesLayer.War war in busi.WarList())
            {
                list.Add(new WarDTO(war));
            }
            return list;
        }

        // GET: api/War/2
        public WarDTO GetWar(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();

            return new WarDTO(busi.getWarById(id));
        }

        // POST: api/War
        public void PostAddWar(WarDTO war)
        {
            War wa = new War(war.Name);
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.addWar(wa);
        }

        // PUT: api/War/2
        public void PutEditWar(WarDTO war)
        {
            War wa = new War(war.ID, war.Name);
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.updateWar(wa);
        }

        // DELETE: api/War/2
        public void Delete(int id)
        {
            BusinessLayer.ThronesTournamentManager busi = new BusinessLayer.ThronesTournamentManager();
            busi.deleteWar(id);
        }
    }
}
