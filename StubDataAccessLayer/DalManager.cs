﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StubDataAccessLayer
{
    public class DalManager
    {
        private List<House> _housesList;
        private List<Character> _characterList;
        private List<Territory> _territoryList;
        // private War _war;

        public DalManager()
        {
            _housesList = new List<House>();
            _characterList = new List<Character>();
            _territoryList = new List<Territory>();

            House Lannister = new House("Lannister", 250);
            House Stark = new House("Stark", 100);
            House Targaryen = new House("Targaryen", 200);

            _housesList.Add(Lannister);
            _housesList.Add(Stark);
            _housesList.Add(Targaryen);

            Character Tyrion = new Character(0, "Tyrion", "Lannister", 100, CharacterType.TACTITIAN, 30, 20, Lannister);
            Character Cercei = new Character(1, "Cercei", "Lannister", 100, CharacterType.LEADER, 40, 90, Lannister);
            Character Arya = new Character(2, "Arya", "Stark", 100, CharacterType.WARRIOR, 70, 20, Stark);
            Character Sansa = new Character(3, "Sansa", "Stark", 100, CharacterType.LOSER, 20, 40, Stark);
            Character Daenerys = new Character(4, "Daenerys", "Targaryen", 100, CharacterType.LEADER, 100, 30, Targaryen);

            _characterList.Add(Tyrion);
            _characterList.Add(Cercei);
            _characterList.Add(Arya);
            _characterList.Add(Sansa);
            _characterList.Add(Daenerys);

            Tyrion.AddRelatives(Cercei, RelationshipType.HATRED);
            Sansa.AddRelatives(Tyrion, RelationshipType.FRIENDSHIP);
            Arya.AddRelatives(Sansa, RelationshipType.LOVE);

            _territoryList.Add(new Territory(0, TerritoryType.MOUNTAIN, Lannister,"Montagne"));
            _territoryList.Add(new Territory(1, TerritoryType.LAND, Stark,"Plaine"));
        }
        public List<House> HousesList()
        {
            return _housesList;
        }

        public List<House> HousesList(int sup)
        {
            List<House> res = new List<House>();
            for (int k = 0; k < _housesList.Count(); k++)
            {
                if (_housesList[k].NbUnits >= sup)
                {
                    res.Add(_housesList[k]);
                }
            }
            return res;
        }
        public List<Territory> TerritoriesList()
        {
            List<Territory> res = new List<Territory>();
            foreach (Territory t in _territoryList)
            {
                res.Add(t);
            }
            return res;
        }
        public List<Character> CharactersList()
        {
            List<Character> res = new List<Character>();
            foreach (Character character in _characterList)
                res.Add(character);
            return res;
        }
        public List<Characteristics> CharactersCharacteristicsList()
        {
            List<Characteristics> res = new List<Characteristics>();
            foreach(Character character in _characterList)
            {
                res.Add(character.Characteristics);
            }
            return res;
        }

    }
}
