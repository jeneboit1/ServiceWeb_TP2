﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ViewModels
{
    class FightsViewModel : ViewModelBase
    {
        public FightsViewModel(List<FightModels> fightsModel)
        {
            _fights = new ObservableCollection<FightModels>();
            foreach (FightModels f in fightsModel)
            {
                _fights.Add(f);
            }
        }

        private ObservableCollection<FightModels> _fights;
        public ObservableCollection<FightModels> Fights
        {
            get { return _fights; }
            private set
            {
                _fights = value;
                OnPropertyChanged("Fights");
            }
        }
    }
}
