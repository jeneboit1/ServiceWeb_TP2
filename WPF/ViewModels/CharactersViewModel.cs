﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ViewModels
{
    class CharactersViewModel : ViewModelBase
    {
        public CharactersViewModel(List<CharacterModels> charactersModel)
        {
            _characters = new ObservableCollection<CharacterModels>();
            foreach (CharacterModels c in charactersModel)
            {
                _characters.Add(c);
            }
        }

        private ObservableCollection<CharacterModels> _characters;
        public ObservableCollection<CharacterModels> Characters
        {
            get { return _characters; }
            private set
            {
                _characters = value;
                OnPropertyChanged("Characters");
            }
        }
    }
}
