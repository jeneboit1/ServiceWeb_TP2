﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ViewModels
{
    class WarsViewModel : ViewModelBase
    {
        public WarsViewModel(List<WarModels> warsModel)
        {
            _wars = new ObservableCollection<WarModels>();
            foreach (WarModels w in warsModel)
            {
                _wars.Add(w);
            }
        }

        private ObservableCollection<WarModels> _wars;
        public ObservableCollection<WarModels> Wars
        {
            get { return _wars; }
            private set
            {
                _wars = value;
                OnPropertyChanged("Wars");
            }
        }
    }
}
