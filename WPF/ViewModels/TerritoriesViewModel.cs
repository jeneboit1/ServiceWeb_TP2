﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ViewModels
{
    class TerritoriesViewModel : ViewModelBase
    {
        public TerritoriesViewModel(List<TerritoryModels> territoriesModel)
        {
            _territories = new ObservableCollection<TerritoryModels>();
            foreach (TerritoryModels t in territoriesModel)
            {
                _territories.Add(t);
            }
        }

        private ObservableCollection<TerritoryModels> _territories;
        public ObservableCollection<TerritoryModels> Territories
        {
            get { return _territories; }
            private set
            {
                _territories = value;
                OnPropertyChanged("Territories");
            }
        }
    }
}
