﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Models;

namespace WPF.ViewModels
{
    class HousesViewModel : ViewModelBase
    {
        public HousesViewModel(List<HouseModels> housesModel)
        {
            _houses = new ObservableCollection<HouseModels>();
            foreach (HouseModels h in housesModel)
            {
                _houses.Add(h);
            }
        }

        private ObservableCollection<HouseModels> _houses;
        public ObservableCollection<HouseModels> Houses
        {
            get { return _houses; }
            private set
            {
                _houses = value;
                OnPropertyChanged("Houses");
            }
        }
    }
}
