﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WPF.Models
{
    public class FightModels
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public HouseModels Challenger1 { get; set; }
        public HouseModels Challenger2 { get; set; }
        public HouseModels Winner { get; set; }
        public WarModels War { get; set; }
        public TerritoryModels Territory { get; set; }

        public FightModels()
        {
            ID = 0;
            Name = "";
            Challenger1 = new HouseModels();
            Challenger2 = new HouseModels();
            Winner = new HouseModels();
            War = new WarModels();
            Territory = new TerritoryModels();
        }
    }
}