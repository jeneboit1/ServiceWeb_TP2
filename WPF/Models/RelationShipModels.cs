﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WPF.Models
{
    public enum RelationshipType
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }

    public class RelationShipModels
    {
        public int CharacterID { get; set; }
        public RelationshipType Type { get; set; }

        public RelationShipModels()
        {
            CharacterID = 0;
            Type = RelationshipType.FRIENDSHIP;
        }

        public RelationShipModels(int characterID, RelationshipType type)
        {
            CharacterID = characterID;
            Type = type;
        }
    }
}