﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Wars.xaml
    /// </summary>
    public partial class Wars : Page
    {
        private Container parent;
        private List<WarModels> WarsList = new List<WarModels>();

        public Wars(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private async Task<List<WarModels>> GetWars()
        {
            List<WarModels> wars = new List<WarModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/war");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    wars = JsonConvert.DeserializeObject<List<WarModels>>(temp);
                }
            }

            return wars;
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            WarsList = await GetWars();
            WarsViewModel wvm = new WarsViewModel(WarsList);
            DataContext = wvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.changeToMainPage();
        }
    }
}
