﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Fights.xaml
    /// </summary>
    public partial class Fights : Page
    {
        private Container parent;
        private List<FightModels> FightsList = new List<FightModels>();

        public Fights(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private async Task<List<FightModels>> GetFights()
        {
            List<FightModels> fights = new List<FightModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/fight");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    fights = JsonConvert.DeserializeObject<List<FightModels>>(temp);
                }
            }

            return fights;
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            FightsList = await GetFights();
            FightsViewModel fvm = new FightsViewModel(FightsList);
            DataContext = fvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.changeToMainPage();
        }
    }
}
