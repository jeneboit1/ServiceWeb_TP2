﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Houses.xaml
    /// </summary>
    public partial class Houses : Page
    {
        private Container parent;
        private List<HouseModels> HousesList = new List<HouseModels>();

        public Houses(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private async Task<List<HouseModels>> GetHouses()
        {
            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }

            return houses;
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            HousesList = await GetHouses();
            HousesViewModel hvm = new HousesViewModel(HousesList);
            DataContext = hvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.changeToMainPage();
        }
    }
}
