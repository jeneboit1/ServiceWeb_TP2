﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Territories.xaml
    /// </summary>
    public partial class Territories : Page
    {
        private Container parent;
        private List<TerritoryModels> TerritoriesList = new List<TerritoryModels>();

        public Territories(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private async Task<List<TerritoryModels>> GetTerritories()
        {
            List<TerritoryModels> territories = new List<TerritoryModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/territory");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    territories = JsonConvert.DeserializeObject<List<TerritoryModels>>(temp);
                }
            }

            return territories;
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            TerritoriesList = await GetTerritories();
            TerritoriesViewModel tvm = new TerritoriesViewModel(TerritoriesList);
            DataContext = tvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.changeToMainPage();
        }
    }
}
