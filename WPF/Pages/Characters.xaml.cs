﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.Models;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Characters.xaml
    /// </summary>
    public partial class Characters : Page
    {
        private Container parent;
        private List<CharacterModels> CharactersList = new List<CharacterModels>();

        public Characters(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private async Task<List<CharacterModels>> GetCharacters()
        {
            List<CharacterModels> characters = new List<CharacterModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    characters = JsonConvert.DeserializeObject<List<CharacterModels>>(temp);
                }
            }

            return characters;
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            CharactersList = await GetCharacters();
            CharactersViewModel cvm = new CharactersViewModel(CharactersList);
            DataContext = cvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.changeToMainPage();
        }
    }
}
