﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF
{
    /// <summary>
    /// Logique d'interaction pour Main.xaml
    /// </summary>
    public partial class Main : Page
    {
        private Container parent;
        public Main(Container p)
        {
            InitializeComponent();
            parent = p;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Characters c = new Characters(parent);
            parent.changePage(c);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Houses h = new Houses(parent);
            parent.changePage(h);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            Wars w = new Wars(parent);
            parent.changePage(w);
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            Fights f = new Fights(parent);
            parent.changePage(f);
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            Territories t = new Territories(parent);
            parent.changePage(t);
        }
    }
}
