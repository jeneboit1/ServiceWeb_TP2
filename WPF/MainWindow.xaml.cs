﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF
{
    public interface Container
    {
        void changeToMainPage();
        void changePage(Page page);
    }

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, Container
    {
        private Main mainPage;
        public MainWindow()
        {
            InitializeComponent();
            mainPage = new Main(this);
            changePage(mainPage);
        }

        public void changeToMainPage()
        {
            _NavigationFrame.Navigate(mainPage);
        }

        public void changePage(Page page)
        {
            _NavigationFrame.Navigate(page);
        }
    }
}
