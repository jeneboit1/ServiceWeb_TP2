﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class WarModels
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public WarModels()
        {
            ID = 0;
            Name = "";
        }
    }
}