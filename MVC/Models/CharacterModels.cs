﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public enum CharacterType
    {
        WARRIOR,
        WITCH,
        TACTITIAN,
        LEADER,
        LOSER
    }
    public class CharacteristicsModels
    {
        public uint PV { get; set; }
        public CharacterType Type { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public CharacteristicsModels()
        {

        }

        public CharacteristicsModels(uint pv, CharacterType type, int bravoury, int crazyness)
        {
            PV = pv;
            Type = type;
            Bravoury = bravoury;
            Crazyness = crazyness;
        }

        public override string ToString()
        {
            return "";
        }
    }

    public class CharacterModels
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public HouseModels House { get; set; }
        public CharacteristicsModels Characteristics { get; set; }
        public List<RelationShipModels> RelationShips { get; set; }

        public CharacterModels()
        {
            ID = 0;
            FirstName = "";
            LastName = "";
            House = new HouseModels();
            Characteristics = new CharacteristicsModels();
            RelationShips = new List<RelationShipModels>();
        }
    }
}