﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace MVC.Controllers
{
    public class WarController : Controller
    {
        // GET: War
        public async Task<ActionResult> Index()
        {
            IEnumerable<WarModels> Wars = new List<WarModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/War");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    Wars = JsonConvert.DeserializeObject<List<WarModels>>(temp);
                }
            }
            return View(Wars);
        }

        // GET: War/Details/0
        public async Task<ActionResult> Details(int id)
        {
            IEnumerable<FightModels> fights = new List<FightModels>();
            IEnumerable<FightModels> Fi = new List<FightModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/war/fight/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    fights = JsonConvert.DeserializeObject<List<FightModels>>(temp);
                }
            }
            Fi = fights.Where(c => c.War.ID==id).ToList();
            ViewBag.Title = (fights.Count() == 0) ? "No Fights for this War" : "All Fights for " + fights.First().War.Name; 
            return View(Fi);
        }

        // GET: War/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: War/Create
        [HttpPost]
        public async Task<ActionResult> Create(WarModels collection)
        {
            try
            {
                WarModels house = new WarModels();
                house.Name = collection.Name;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    String jsonString = JsonConvert.SerializeObject(house);
                    StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/war", content);
                }
                    return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: War/Edit/1
        public async Task<ActionResult> Edit(int id)
        {
            WarModels war = new WarModels();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/war/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    war = JsonConvert.DeserializeObject<WarModels>(temp);
                }
            }
            return View(war);
        }

        [HttpPost]
        // POST: House/Edit/5
        public async Task<ActionResult> Edit(int id, WarModels collection)
        {

            WarModels war = new WarModels();
            war.ID = id;
            war.Name = collection.Name;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(war);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("api/war", content);
                return RedirectToAction("Index");
            }
        }

        // GET: War/Delete/1
        public async Task<ActionResult> Delete(int id)
        {
            WarModels war = new WarModels();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/war/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    war = JsonConvert.DeserializeObject<WarModels>(temp);
                }
            }
            return View(war);
        }

        // POST: Fight/Delete/2
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.DeleteAsync("api/war/" + id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
