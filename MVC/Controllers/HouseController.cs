﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace MVC.Controllers
{
    public class HouseController : Controller
    {
        // GET: House
            public async Task<ActionResult> Index()
        {
            IEnumerable<HouseModels> Houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/House");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    Houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            return View(Houses);
        }


        // GET: House/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: House/Create
        [HttpPost]
        public async Task<ActionResult> Create(HouseModels collection)
        {

            try
            {
                HouseModels house = new HouseModels();
                house.Name = collection.Name;
                house.NbUnits = collection.NbUnits;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    String jsonString = JsonConvert.SerializeObject(house);
                    StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/House", content);
                    return RedirectToAction("Index");

                }
            }

            catch
            {
                return View();

            }

        }


        public async Task<ActionResult> Edit(int id)
        {
            HouseModels house = new HouseModels();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    house = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }
            return View(house);
        }

        [HttpPost]
        // POST: House/Edit/5
        public async Task<ActionResult> Edit(int id, HouseModels collection)
        {

            HouseModels house = new HouseModels();
            house.ID = id;
            house.Name = collection.Name;
            house.NbUnits = collection.NbUnits;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(house);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("api/house", content);
                return RedirectToAction("Index");
            }
        }
        // POST: House/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            HouseModels house = new HouseModels();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    house = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }
            return View(house);
        }

        // POST: Fight/Delete/2
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.DeleteAsync("api/house/" + id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
