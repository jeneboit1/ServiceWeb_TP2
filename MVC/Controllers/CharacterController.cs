﻿using System;
using System.Net;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace MVC.Controllers
{
    public class CharacterController : Controller
    {
        // GET: Character
        public async Task<ActionResult> Index()
        {
            IEnumerable<CharacterModels> characters = new List<CharacterModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    characters = JsonConvert.DeserializeObject<IEnumerable<CharacterModels>>(temp);
                }
            }
            return View(characters);
        }

        // GET: Character/Details/5
        public async Task<ActionResult> Details(int id)
        {
            CharacterModels character = new CharacterModels();
            IEnumerable<CharacterModels> relationships = new List<CharacterModels>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    character = JsonConvert.DeserializeObject<CharacterModels>(temp);
                }
            }

            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            return View(character);
        }

        // GET: Character/Create
        public async Task<ActionResult> Create()
        {
            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            ViewBag.ListHouses = houses.Select(h => new SelectListItem()
            {
                Text = h.Name,
                Value = h.ID.ToString()
            });
            return View();
        }

        // POST: Character/Create
        [HttpPost]
        public async Task<ActionResult> Create(CharacterModels collection)
        {  
            CharacterModels perso = new CharacterModels();
            perso.FirstName = collection.FirstName;
            perso.LastName = collection.LastName;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + collection.House.ID);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    perso.House = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }

            perso.Characteristics.Type = collection.Characteristics.Type;
            perso.Characteristics.PV = collection.Characteristics.PV;
            perso.Characteristics.Bravoury = collection.Characteristics.Bravoury;
            perso.Characteristics.Crazyness = collection.Characteristics.Crazyness;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(perso);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("api/character", content);
            }
            return RedirectToAction("Index");

        }
        // GET: Character/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            List<HouseModels> houses = new List<HouseModels>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    houses = JsonConvert.DeserializeObject<List<HouseModels>>(temp);
                }
            }
            CharacterModels character = new CharacterModels();
            IEnumerable<CharacterModels> relationships = new List<CharacterModels>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    character = JsonConvert.DeserializeObject<CharacterModels>(temp);
                }
            }

            ViewBag.ListHouses = houses.Select(h => new SelectListItem()
            {
                Text = h.Name,
                Value = h.ID.ToString()
            });
            return View(character);
        }

        // POST: Character/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, CharacterModels collection)
        {
            CharacterModels perso = new CharacterModels();
            perso.ID = collection.ID;
            perso.FirstName = collection.FirstName;
            perso.LastName = collection.LastName;
            perso.Characteristics.PV = collection.Characteristics.PV;
            perso.Characteristics.Bravoury = collection.Characteristics.Bravoury;
            perso.Characteristics.Crazyness = collection.Characteristics.Crazyness;
            perso.Characteristics.Type = collection.Characteristics.Type;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house/" + collection.House.ID);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    perso.House = JsonConvert.DeserializeObject<HouseModels>(temp);
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                String jsonString = JsonConvert.SerializeObject(perso);
                StringContent content = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("api/character", content);
            }
            return RedirectToAction("Index");
        }

        // GET: Character/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            CharacterModels character = new CharacterModels();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:13666/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character/" + id);
                if (response.IsSuccessStatusCode)
                {
                    string temp = await response.Content.ReadAsStringAsync();
                    character = JsonConvert.DeserializeObject<CharacterModels>(temp);
                }
            }
            return View(character);
        }

        // POST: Fight/Delete/2
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:13666/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.DeleteAsync("api/character/" + id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
