﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThronesTournamentConsole
{
    class Program
    {
        static BusinessLayer.ThronesTournamentManager businessLayer = new BusinessLayer.ThronesTournamentManager();

        public void DisplayHouses()
        {
            Console.WriteLine("---- Display Houses ----");
            List<String> _listHouse = businessLayer.HousesList().Select(House => House.Name + " has " + House.NbUnits + " Units.").ToList();
            for (int k = 0; k < _listHouse.Count(); k++)
                Console.WriteLine(_listHouse[k]);
        }
        public void DisplayHouses(int sup)
        {
            Console.WriteLine("---- Display Houses with Units > " + sup + " ----");
            List<String> _listHouse = businessLayer.HousesList(sup).Select(House => House.Name).ToList();
            for (int k = 0; k < _listHouse.Count(); k++)
                Console.WriteLine(_listHouse[k]);
        }
        public void DisplayCharacteristics()
        {
            Console.WriteLine("---- Display Characteristics ----");
            foreach (String s in businessLayer.CharactersCharacteristicsList().Select(c => "PV -> " + c.PV + ", Type -> " + c.Type + ", Bravoury -> " + c.Bravoury + ", Crazyness -> " + c.Crazyness).ToList())
                Console.WriteLine(s);
        }

        public void DisplayCharacters()
        {
            Console.WriteLine("---- Display Characters ----");
            foreach (String s in businessLayer.CharactersList().Select(c => c.FirstName + " " + c.LastName).ToList())
                Console.WriteLine(s);
        }
       
        public void DisplayCharacters(int BravourySup, int PVSup)
        {
            Console.WriteLine("---- Display Characters with Bravoury > " + BravourySup + " and Pv > " + PVSup + " ----");
            foreach (String s in businessLayer.CharactersList(BravourySup, PVSup).Select(c => c.FirstName + " " + c.LastName).ToList())
                Console.WriteLine(s);
        }
        public void DisplayTerritories()
        {
            Console.WriteLine("---- Display Territories ----");
            List<String> terr = businessLayer.TerritoryList().Select(t => t.Type + " owned by " + t.Owner.Name).ToList();
            for (int k = 0; k < terr.Count(); k++)
                Console.WriteLine(terr[k]);
        }
        public void DisplayFights()
        {
            Console.WriteLine("---- Display Fight ----");
            List<String> terr = businessLayer.FightsList().Select(t => t.Name + " -> " + t.Challenger1.Name + " vs " + t.Challenger2.Name + " in the war " + t.War.Name).ToList();
            for (int k = 0; k < terr.Count(); k++)
                Console.WriteLine(terr[k]);
        }
        public int addCharacter()
        {
            House house = businessLayer.getHouseById(1);
            return businessLayer.addCharacter(new Character("Robb", "Stark", 100, CharacterType.LEADER, 90, 20, house));
        }

        public void deleteCharacter(int id)
        {
            businessLayer.deleteCharacater(id);
        }       

        public void updateCharacter()
        {
            Character character = businessLayer.getCharacterById(6);
            character.Characteristics.Bravoury = 90;
            businessLayer.updateCharacter(character);
        }

        static void Main(string[] args)
        {
            Program game = new Program();
            House house = new House("", 250);
            House house2 = new House("", 100);
            War war = new War("");
            Fight fight = new Fight("", house, house2, house, war);
            businessLayer.addFight(fight);
            Console.ReadKey();
        }
    }
}
